# Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


PROJECT_NAME=file-check

# Folders and files
SRC_DIR=src
INC_DIR=src
INC_NAMESPACE_DIR=$(INC_DIR)
OBJ_DIR=obj
BIN_DIR=bin
RELEASE_DIR=$(BIN_DIR)
DEBUG_DIR=$(BIN_DIR)
DOC_DIR=doc
CHK_DIR=tests
SYS_BIN_DIR=/usr/local/bin

# Compilation
CC=gcc
INCLUDES=-I$(INC_DIR)
CFLAGS_WARNING_YES=-Wall -Wextra -Wpedantic -Wformat=2 -Werror
CFLAGS_WARNING_NO=-Wno-unused-command-line-argument
CFLAGS_WARNING=$(CFLAGS_WARNING_YES) $(CFLAGS_WARNING_NO)
CFLAGS_SECURITY_PI=-fPIC -fPIE -pie
CFLAGS_SECURITY_STACK=-fstack-protector-all
CFLAGS_SECURITY_COMPILE=$(CFLAGS_SECURITY_PI) $(CFLAGS_SECURITY_STACK) -D_FORTIFY_SOURCE=2
CFLAGS_SECURITY_LINK=-Wl,-z,relro -Wl,-z,now
CFLAGS_SECURITY=$(CFLAGS_SECURITY_COMPILE) $(CFLAGS_SECURITY_LINK)
CFLAGS=-std=c99 $(CFLAGS_WARNING) $(CFLAGS_SECURITY) -O2 -s $(INCLUDES)

# Commands
CMAKE=cmake -Wdev
HAS_GCC=$(shell command -v gcc 2> /dev/null)
HAS_CLANG=$(shell command -v clang 2> /dev/null)
EXE_NAME=$(PROJECT_NAME)
EXE=$(BIN_DIR)/$(EXE_NAME)
EXE_CHECK=$(EXE) --quiet --no-cr --newline-at-eof \
	--max-line-length 80 --max-nb-lines 700

# Package and archive
PACKAGE=$(PROJECT_NAME)
FILES_TO_BUILD=$(SRC_DIR)/ $(INC_DIR)/ $(CHK_DIR)/ makefile
FILES_TO_ARCHIVE=$(FILES_TO_BUILD) \
	licenses/ *.md \
	.gitignore .editorconfig .dir-locals.el \
	.gitlab-ci.yml


default: bin


install: $(EXE)
	mv -- $(EXE) $(SYS_BIN_DIR)

uninstall:
	$(RM) -f -- $(SYS_BIN_DIR)/$(EXE_NAME)


bin: $(EXE)

$(BIN_DIR)/file-check: \
		$(SRC_DIR)/specific/file-check.c \
		$(INC_DIR)/utils/*.h $(INC_DIR)/specific/*.h
	@mkdir -p $(BIN_DIR)
	$(CC) $(CFLAGS) \
		$(SRC_DIR)/specific/file-check.c \
		-o $(BIN_DIR)/file-check


check: tests

tests: tests-except-compilers tests-compilers

tests-except-compilers: tests-static tests-dynamic

tests-static: tests-static-except-hardening tests-hardening

tests-static-except-hardening: bin tests-static-src

tests-static-src: tests-cppcheck tests-file-test

tests-cppcheck:
	cppcheck \
		--verbose --quiet --error-exitcode=1 \
		--language=c --std=c99 --force \
		--enable=warning,style,performance,portability \
		$(SRC_DIR) $(INC_DIR) $(CHK_DIR)

tests-file-test:
	$(EXE_CHECK) -- $(INC_DIR)/utils/*.h $(INC_DIR)/specific/*.c
	$(EXE_CHECK) -- $(SRC_DIR)/specific/*.c # $(SRC_DIR)/utils/*.c
#	$(EXE_CHECK) -- $(CHK_DIR)/*.h $(CHK_DIR)/*.c
	$(EXE_CHECK) --ascii $(INC_DIR)/utils/bool.h
	$(EXE_CHECK) --ascii $(INC_DIR)/utils/vector_generic.h

tests-hardening:
	if command -v hardening-check > /dev/null; then \
		hardening-check --quiet --verbose $(EXE); \
		fi

tests-dynamic: tests-valgrind tests-files

tests-valgrind: bin
	valgrind --quiet --errors-for-leak-kinds=all --error-exitcode=1 \
		$(EXE_CHECK) --ascii --newline-at-eof $(INC_DIR)/utils/bool.h \
		1> /dev/null


tests-compilers: tests-gcc tests-clang

tests-gcc: bin-gcc
#	cd build-gcc   && make test

tests-clang: bin-clang
#	cd build-clang && make test

bin-gcc: build-gcc/Makefile
	cd build-gcc   && make

bin-clang: build-clang/Makefile
	cd build-clang && make

build-gcc/Makefile: $(FILES_TO_BUILD)
ifndef HAS_GCC
	$(error "GCC is not available please install it")
endif
	@mkdir -p build-gcc
	cd build-gcc   && CC=gcc   $(CMAKE) -DCMAKE_BUILD_TYPE=release ..

build-clang/Makefile: $(FILES_TO_BUILD)
ifndef HAS_CLANG
	$(error "Clang is not available please install it")
endif
	@mkdir -p build-clang
	cd build-clang && CC=clang $(CMAKE) -DCMAKE_BUILD_TYPE=release ..


tests-files: tests-files-tests tests-files-others

tests-files-tests: tests-files-tests-conditions tests-files-tests-multiple

tests-files-tests-conditions: tests-files-tests-nb-lines
	$(EXE) --quiet --empty $(CHK_DIR)/empty_true.txt
	! $(EXE) --quiet --empty $(CHK_DIR)/empty_false.txt
	! $(EXE) --quiet --empty -- \
		$(CHK_DIR)/empty_true.txt $(CHK_DIR)/empty_false.txt
	! $(EXE) --quiet --empty -- \
		$(CHK_DIR)/empty_false.txt $(CHK_DIR)/empty_true.txt
	$(EXE) --quiet --not-empty $(CHK_DIR)/empty_false.txt
	! $(EXE) --quiet --not-empty $(CHK_DIR)/empty_true.txt
	! $(EXE) --quiet --not-empty -- \
		$(CHK_DIR)/empty_true.txt $(CHK_DIR)/empty_false.txt
	! $(EXE) --quiet --not-empty -- \
		$(CHK_DIR)/empty_false.txt $(CHK_DIR)/empty_true.txt
	! $(EXE) --quiet --max-line-length fail $(CHK_DIR)/empty_true.txt
	$(EXE) --ascii --no-cr --max-line-length 0 $(CHK_DIR)/empty_true.txt
	! $(EXE) -q --max-line-length 0 $(CHK_DIR)/empty_false.txt
	$(EXE_CHECK) --ascii $(CHK_DIR)/ascii_true.txt
	! $(EXE_CHECK) --ascii $(CHK_DIR)/ascii_false.txt
	! $(EXE_CHECK) --no-cr $(CHK_DIR)/carriage-return_true.txt
	$(EXE_CHECK) --no-cr $(CHK_DIR)/carriage-return_false.txt
	$(EXE_CHECK) --newline-at-eof $(CHK_DIR)/newline-at-eof_true.txt
	! $(EXE_CHECK) --newline-at-eof $(CHK_DIR)/newline-at-eof_false.txt

tests-files-tests-nb-lines: bin
	! $(EXE) -q --max-nb-lines fail $(CHK_DIR)/nb-lines-1_true.txt
	! $(EXE) -q --max-nb-lines 0 $(CHK_DIR)/nb-lines-1_true.txt
	$(EXE) --max-nb-lines 1 $(CHK_DIR)/nb-lines-1_true.txt
	$(EXE) --max-nb-lines 2 $(CHK_DIR)/nb-lines-1_true.txt
	! $(EXE) -q --max-nb-lines 1 $(CHK_DIR)/nb-lines-1_false.txt
	$(EXE) --max-nb-lines 2 $(CHK_DIR)/nb-lines-2_true.txt
	$(EXE) --max-nb-lines 3 $(CHK_DIR)/nb-lines-2_true.txt
	! $(EXE) -q --max-nb-lines 1 $(CHK_DIR)/nb-lines-2_true.txt
	$(EXE) --max-nb-lines 3 $(CHK_DIR)/nb-lines-3_true.txt
	$(EXE) --max-nb-lines 4 $(CHK_DIR)/nb-lines-3_true.txt
	! $(EXE) -q --max-nb-lines 2 $(CHK_DIR)/nb-lines-3_true.txt
	$(EXE) --max-nb-lines-all-files 5 -- \
		$(CHK_DIR)/nb-lines-2_true.txt $(CHK_DIR)/nb-lines-3_true.txt
	! $(EXE) -q --max-nb-lines-all-files 4 -- \
		$(CHK_DIR)/nb-lines-2_true.txt $(CHK_DIR)/nb-lines-3_true.txt

tests-files-tests-multiple: bin
	! $(EXE) -q --ascii --
	! $(EXE) -q --ascii --files
	$(EXE) --ascii -- $(CHK_DIR)/empty_true.txt $(CHK_DIR)/ascii_true.txt
	$(EXE) --ascii --files $(CHK_DIR)/empty_true.txt $(CHK_DIR)/ascii_true.txt
	! $(EXE) -q --ascii -- $(CHK_DIR)/ascii_true.txt $(CHK_DIR)/ascii_false.txt
	! $(EXE) -q --ascii -- $(CHK_DIR)/ascii_false.txt $(CHK_DIR)/ascii_true.txt

tests-files-others:
	! $(EXE) --quiet 2> /dev/null
	! $(EXE_CHECK)
	! $(EXE_CHECK) a


archives: zip tar-gz tar-bz2 tar-xz

dist: default-archive

default-archive: tar-xz

zip: $(PACKAGE).zip

$(PACKAGE).zip: $(FILES_TO_ARCHIVE)
	zip $(PACKAGE).zip -r -- $(FILES_TO_ARCHIVE)

tar-gz: $(PACKAGE).tar.gz

$(PACKAGE).tar.gz: $(FILES_TO_ARCHIVE)
	tar -zcvf $(PACKAGE).tar.gz -- $(FILES_TO_ARCHIVE)

tar-bz2: $(PACKAGE).tar.bz2

$(PACKAGE).tar.bz2: $(FILES_TO_ARCHIVE)
	tar -jcvf $(PACKAGE).tar.bz2 -- $(FILES_TO_ARCHIVE)

tar-xz: $(PACKAGE).tar.xz

$(PACKAGE).tar.xz: $(FILES_TO_ARCHIVE)
	tar -cJvf $(PACKAGE).tar.xz -- $(FILES_TO_ARCHIVE)


clean: \
	clean-build clean-externals clean-bin clean-compile-tmp \
	clean-python clean-profiling clean-cmake clean-ide \
	clean-tmp clean-doc clean-latex clean-tests clean-archives

clean-build: clean-build-release clean-build-debug
	@$(RM) -rf -- build*/ Build*/ builds*/ Builds*/

clean-build-release:
	@$(RM) -rf -- release/ Release/ $(RELEASE_DIR)/

clean-build-debug:
	@$(RM) -rf -- dbg/ debug/ Debug/ $(DEBUG_DIR)/

clean-externals:
	git submodule update

clean-bin:
	$(RM) -rf -- \
		*.o *.a *.so *.ko *.lo *.dll *.out \
		bin/ BIN/ binaries/

clean-compile-tmp:
	$(RM) -f -- \
		*.asm *.ihx *.lk *.lst *.map *.mem \
		*.rel *.rst *.s *.sym

clean-python:
	$(RM) -rf -- *.pyc __pycache__

clean-profiling:
	$(RM) -rf -- callgrind.out.*

clean-cmake:
	$(RM) -rf -- \
		CMakeLists.txt.user CMakeCache.txt \
		CMakeFiles/ cmake_install.cmake \
		CPackConfig.cmake CPackSourceConfig.cmake \
		ctest.cmake CTest.cmake \
		CTestfile CTestFile \
		CTestfile.cmake CTestFile.cmake \
		CTestTestfile CTestTestFile \
		CTestTestfile.cmake CTestTestFile.cmake

clean-ide: clean-qt-creator clean-codeblocks

clean-qt-creator:
	$(RM) -f -- qmake_makefile *.pro.user

clean-codeblocks:
	$(RM) -f -- *.cbp *.CBP

clean-tmp:
	$(RM) -rf -- \
		*~ .\#* \#* \
		*.swp *.swap *.SWP *.SWAP \
		*.bak *.backup *.BAK *.BACKUP \
		*.sav *.save *.SAV *.SAVE \
		*.autosav *.autosave \
		*.log *.log.* error_log* \
		.cache/ .thumbnails/ \
		.CACHE/ .THUMBNAILS/

clean-doc:
	@$(RM) -rf -- \
		doc/ Doc/ docs/ Docs/ \
		documentation/ Documentation/

clean-latex:
	$(RM) -f -- \
		*.pdf *.dvi *.ps \
		*.PDF *.DVI *.PS \
		*.acn *.aux *.bcf *.cut *.fls *.glo *.ist *.lof *.nav \
		*.run.xml *.snm *.toc *.vrb *.vrm *.xdy \
		*.fdb_latexmk *-converted-to.* \
		*.synctex *.synctex.gz *.synctex.bz2 *.synctex.bzip *.synctex.xz \
		*.synctex.zip *.synctex.7z *.synctex.rar

clean-tests: clean-tests-folders clean-tests-files

clean-tests-folders:
	$(RM) -rf -- tests-gcc/ tests-clang/ tests-llvm/

clean-tests-files:
	$(RM) -rf -- \
		a b c a.* b.* c.* \
		test.c tests.c \
		test.cc tests.cc test.cpp tests.cpp \
		test.java tests.java \
		test.py tests.py \
		junit* report* TEST*

clean-archives:
	$(RM) -rf -- \
		*.deb *.rpm *.exe *.msi *.dmg *.apk *.ipa \
		*.DEB *.RPM *.EXE *.MSI *.DMG *.APK *.IPA \
		*.tar.* *.tgz *.gz *.bz2 *.lz *.lzma *.xz \
		*.TAR.* *.TGZ *.GZ *.BZ2 *.LZ *.LZMA *.XZ \
		*.zip *.7z *.rar *.jar \
		*.ZIP *.7Z *.RAR *.JAR \
		*.iso *.ciso *.img *.gcz *.wbfs \
		*.ISO *.CISO *.IMG *.GCZ *.WBFS

clean-git:
	git clean -fdx
