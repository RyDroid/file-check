# file-checker

It is a program to do check(s) on file(s).
For example, it can check the number of lines or if there is a new line a the end of a file.

It is [free/libre software](https://www.gnu.org/philosophy/free-sw.html).
It works at least on GNU/Linux.
Happy hacking and enjoy!

It currently only has a command line interface.
There is no plan to create a GUI.
But you can do it as a side project.

## Build

You need a C99 compiler (like GCC or Clang) and C library.
If you have `make`, use it.

## Modify

All text files have to be in [UTF-8](https://en.wikipedia.org/wiki/UTF-8) (without [BOM](https://en.wikipedia.org/wiki/Byte_order_mark)) encoding (generally UTF-8 on GNU/Linux and [ISO 8859-15](https://en.wikipedia.org/wiki/ISO/IEC_8859-15) on MS Windows) and LF [character newline](https://en.wikipedia.org/wiki/Newline) (sometime called UNIX newline/endline) (Windows generally used CRLF).
Unfortunately, by default some text editors save with the same encoding and character newline of your OS.

An empty line must be at the end of text files.
The aim is readability with [`cat file`](https://en.wikipedia.org/wiki/Cat_%28Unix%29).

We use english, so please use it everywhere in the project (messages, function names, doc, etc).

Names of variables, functions/methods, classes and everything else have to be clear, even if the name is a little longer.
Names of variables should not be a name of a type.

As much as possible, the source code should be a good documentation.
Otherwise, you have to create documentation.

If you need to make a structured document, you should consider [Markdown](https://en.wikipedia.org/wiki/Markdown).
For example, this document uses the Markdown syntax.
For longer texts or presentations, [LaTeX](http://latex-project.org/) and HTML+CSS(+JS) could be good options.

You must release your contributions under free/libre license(s) if you want them to be accepted upstream.

## Get newer versions

You may find a new version with [the repository rydroid/file-check on GitLab.com](https://gitlab.com/RyDroid/file-check).

## License and authors

See [LICENSE.md](LICENSE.md) and logs of git for the full list of contributors of the project.
