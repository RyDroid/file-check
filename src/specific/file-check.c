/*
 * Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "specific/empty_checker.h"
#include "specific/ascii_checker.h"
#include "specific/no_carriage_return_checker.h"
#include "specific/newline_at_eof_checker.h"
#include "utils/cstring.h"
#include "specific/character_checkers.h"


struct progress_size_t
{
  size_t current;
  size_t max;
};
typedef struct progress_size_t progress_size_t;

struct line_length_max_data_t
{
  progress_size_t progress;
  size_t          line;
};
typedef struct line_length_max_data_t line_length_max_data_t;

static inline
bool
line_length_max_checker_handler(int   a_character,
				void* data)
{
  line_length_max_data_t* data_typed = (line_length_max_data_t*) data;
  if(is_newline(a_character) || a_character == EOF)
    {
      if(data_typed->progress.current > data_typed->progress.max)
	{
	  return false;
	}
      data_typed->progress.current = 0;
      ++data_typed->line;
      return true;
    }
  ++data_typed->progress.current;
  return true;
}

bool
line_length_max_checker_error_printer(FILE* stream,
				      const void* data)
{
  if(stream == NULL || data == NULL)
    {
      return false;
    }
  const line_length_max_data_t* data_typed = (line_length_max_data_t*) data;
  fprintf(stream,
	  "line %zu: "
	  "%zu (current length) > %zu (max length)\n",
	  data_typed->line,
	  data_typed->progress.current, data_typed->progress.max);
  return true;
}

bool
line_length_max_checker_init(void* data)
{
  if(data == NULL)
    {
      return false;
    }
  
  line_length_max_data_t* data_typed = (line_length_max_data_t*) data;
  data_typed->progress.current = 0;
  data_typed->line = 1;
  return true;
}

character_checker_t
line_length_max_checker_create(size_t length_max)
{
  character_checker_t checker;
  character_checker_init_first_time_only_unsafe(&checker);
  checker.data          = malloc(sizeof(line_length_max_data_t));
  if(checker.data != NULL)
    {
      line_length_max_data_t* data = (line_length_max_data_t*) checker.data;
      data->progress.max = length_max;
    }
  checker.data_free     = free;
  checker.init          = line_length_max_checker_init;
  checker.handler       = line_length_max_checker_handler;
  checker.error_printer = line_length_max_checker_error_printer;
  return checker;
}

static inline
bool
nb_lines_max_checker_handler(int   a_character,
			     void* data)
{
  progress_size_t* data_typed = (progress_size_t*) data;
  if(is_newline(a_character))
    {
      ++data_typed->current;
    }
  return data_typed->current <= data_typed->max;
}

bool
nb_lines_max_checker_error_printer(FILE* stream,
				   const void* data)
{
  if(stream == NULL || data == NULL)
    {
      return false;
    }
  const progress_size_t* data_typed = (progress_size_t*) data;
  fprintf(stream,
	  "Number of lines: %zu (current) > %zu (max)\n",
	  data_typed->current, data_typed->max);
  return true;
}

bool
nb_lines_max_checker_per_file_init(void* data)
{
  if(data == NULL)
    {
      return false;
    }
  
  progress_size_t* data_typed = (progress_size_t*) data;
  data_typed->current = 1;
  return true;
}

bool
nb_lines_max_checker_all_files_init(void* data)
{
  if(data == NULL)
    {
      return false;
    }
  
  progress_size_t* data_typed = (progress_size_t*) data;
  data_typed->current += 1;
  return true;
}

character_checker_t
nb_lines_max_checker_per_file_create(size_t length_max)
{
  character_checker_t checker;
  character_checker_init_first_time_only_unsafe(&checker);
  checker.data          = malloc(sizeof(progress_size_t));
  if(checker.data != NULL)
    {
      progress_size_t* data = (progress_size_t*) checker.data;
      data->max         = length_max;
    }
  checker.data_free     = free;
  checker.init          = nb_lines_max_checker_per_file_init;
  checker.handler       = nb_lines_max_checker_handler;
  checker.error_printer = nb_lines_max_checker_error_printer;
  return checker;
}

character_checker_t
nb_lines_max_checker_all_files_create(size_t length_max)
{
  character_checker_t checker;
  checker = nb_lines_max_checker_per_file_create(length_max);
  ((progress_size_t*) checker.data)->current = 0;
  checker.init = nb_lines_max_checker_all_files_init;
  return checker;
}


int
main(int argc, const char* argv[])
{
  if(argc < 2)
    {
      fprintf(stderr, "No argument!\n");
      return EXIT_FAILURE;
    }
  
  bool quiet = false;
  character_checkers_t character_checkers;
  character_checkers_init_first_time_only(&character_checkers);
  int i;
  for(i=1; i < argc - 1; ++i)
    {
      if(cstring_equals_unsafe(argv[i], "--") ||
	 cstring_equals_unsafe(argv[i], "--files"))
	{
	  ++i;
	  break;
	}
      else if(cstring_equals_unsafe(argv[i], "-q") ||
	      cstring_equals_unsafe(argv[i], "--quiet"))
	{
	  quiet = true;
	}
      else if(cstring_equals_unsafe(argv[i], "--verbose"))
	{
	  quiet = false;
	}
      else if(cstring_equals_unsafe(argv[i], "--empty") ||
	      cstring_equals_unsafe(argv[i], "--empty=true") ||
	      cstring_equals_unsafe(argv[i], "--not-empty=false"))
	{
	  if(!character_checkers_add(&character_checkers,
				     empty_checker_create())
	     && !quiet)
	    {
	      fprintf(stderr,
		      "There was already an empty checker.\n");
	    }
	}
      else if(cstring_equals_unsafe(argv[i], "--not-empty") ||
	      cstring_equals_unsafe(argv[i], "--not-empty=true") ||
	      cstring_equals_unsafe(argv[i], "--empty=false"))
	{
	  if(!character_checkers_add(&character_checkers,
				     not_empty_checker_create())
	     && !quiet)
	    {
	      fprintf(stderr,
		      "There was already a not empty checker.\n");
	    }
	}
      else if(cstring_equals_unsafe(argv[i], "--ascii") ||
	      cstring_equals_unsafe(argv[i], "--ascii-only"))
	{
	  if(!character_checkers_add(&character_checkers,
				     ascii_checker_create())
	     && !quiet)
	    {
	      fprintf(stderr,
		      "There was already a ASCII checker.\n");
	    }
	}
      else if(cstring_equals_unsafe(argv[i], "--no-cr") ||
	      cstring_equals_unsafe(argv[i], "--no-carriage-return"))
	{
	  if(!character_checkers_add(&character_checkers,
				     no_carriage_return_checker_create())
	     && !quiet)
	    {
	      fprintf(stderr,
		      "There was already a no carriage-return checker.\n");
	    }
	}
      else if(cstring_equals_unsafe(argv[i], "--newline-at-eof"))
	{
	  if(!character_checkers_add(&character_checkers,
				     newline_at_eof_checker_create())
	     && !quiet)
	    {
	      fprintf(stderr,
		      "There was already a newline at end-of-file checker.\n");
	    }
	}
      else if(cstring_equals_unsafe(argv[i], "--max-line-length") ||
	      cstring_equals_unsafe(argv[i], "--line-length-max"))
	{
	  if(++i >= argc - 1)
	    {
	      character_checkers_destructor(&character_checkers);
	      if(!quiet)
		{
		  fprintf(stderr,
			  "%s need a value as the following argument.\n",
			  argv[i-1]);
		}
	      return EXIT_FAILURE;
	    }
	  
	  bool success;
	  int number = cstring_to_int(argv[i], &success);
	  if(!success)
	    {
	      character_checkers_destructor(&character_checkers);
	      if(!quiet)
		{
		  fprintf(stderr,
			  "%s does not seem to be a number.\n",
			  argv[i]);
		}
	      return EXIT_FAILURE;
	    }
	  
	  if(number < 0)
	    {
	      character_checkers_destructor(&character_checkers);
	      if(!quiet)
		{
		  fprintf(stderr,
			  "A line can not have less than 0 character.\n");
		}
	      return EXIT_FAILURE;
	    }
	  size_t max = (size_t) number;
	  
	  if(!character_checkers_add(&character_checkers,
				     line_length_max_checker_create(max))
	     && !quiet)
	    {
	      fprintf(stderr,
		      "There was already a line length checker.\n");
	    }
	}
      else if(cstring_equals_unsafe(argv[i], "--max-nb-lines") ||
	      cstring_equals_unsafe(argv[i], "--nb-lines-max") ||
	      cstring_equals_unsafe(argv[i], "--max-nb-lines-per-file") ||
	      cstring_equals_unsafe(argv[i], "--nb-lines-max-per-file"))
	{
	  if(++i >= argc - 1)
	    {
	      character_checkers_destructor(&character_checkers);
	      if(!quiet)
		{
		  fprintf(stderr,
			  "%s need a value as the following argument.\n",
			  argv[i-1]);
		}
	      return EXIT_FAILURE;
	    }
	  
	  bool success;
	  int number = cstring_to_int(argv[i], &success);
	  if(!success)
	    {
	      character_checkers_destructor(&character_checkers);
	      if(!quiet)
		{
		  fprintf(stderr,
			  "%s does not seem to be a number.\n",
			  argv[i]);
		}
	      return EXIT_FAILURE;
	    }
	  
	  if(number < 1)
	    {
	      character_checkers_destructor(&character_checkers);
	      if(!quiet)
		{
		  fprintf(stderr,
			  "A file can not have less than 1 line.\n");
		}
	      return EXIT_FAILURE;
	    }
	  size_t max = (size_t) number;
	  
	  if(!character_checkers_add(&character_checkers,
				     nb_lines_max_checker_per_file_create(max))
	     && !quiet)
	    {
	      fprintf(stderr,
		      "There was already a number of lines per file checker.\n");
	    }
	}
      else if(cstring_equals_unsafe(argv[i], "--max-nb-lines-all-files") ||
	      cstring_equals_unsafe(argv[i], "--nb-lines-max-all-files"))
	{
	  if(++i >= argc - 1)
	    {
	      character_checkers_destructor(&character_checkers);
	      if(!quiet)
		{
		  fprintf(stderr,
			  "%s need a value as the following argument.\n",
			  argv[i-1]);
		}
	      return EXIT_FAILURE;
	    }
	  
	  bool success;
	  int number = cstring_to_int(argv[i], &success);
	  if(!success)
	    {
	      character_checkers_destructor(&character_checkers);
	      if(!quiet)
		{
		  fprintf(stderr,
			  "%s does not seem to be a number.\n",
			  argv[i]);
		}
	      return EXIT_FAILURE;
	    }
	  
	  if(number < 1)
	    {
	      character_checkers_destructor(&character_checkers);
	      if(!quiet)
		{
		  fprintf(stderr,
			  "A file can not have less than 1 line.\n");
		}
	      return EXIT_FAILURE;
	    }
	  size_t max = (size_t) number;
	  
	  if(!character_checkers_add(&character_checkers,
				     nb_lines_max_checker_all_files_create(max))
	     && !quiet)
	    {
	      fprintf(stderr,
		      "There was already "
		      "a checker for the number of lines of all files.\n");
	    }
	}
      else if(cstring_equals_unsafe(argv[i], "--debug-print-argv"))
	{
	  for(i=0; i < argc; ++i)
	    {
	      puts(argv[i]);
	    }
	  return EXIT_SUCCESS;
	}
      else
	{
	  character_checkers_destructor(&character_checkers);
	  if(!quiet)
	    {
	      fprintf(stderr, "%s is not a managed argument.\n", argv[i]);
	    }
	  return EXIT_FAILURE;
	}
    }
  
  for(; i < argc; ++i)
    {
      const char* file_name = argv[i];
      if(!character_checkers_handle_file_with_name(&character_checkers,
						   file_name,
						   quiet ? NULL : stderr))
	{
	  if(!quiet)
	    {
	      fputs(file_name, stderr);
	      fputc('\n',      stderr);
	    }
	  character_checkers_destructor(&character_checkers);
	  return EXIT_FAILURE;
	}
    }
  
  character_checkers_destructor(&character_checkers);
  return EXIT_SUCCESS;
}
