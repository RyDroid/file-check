/*
 * Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef FILE_CHECK_NEWLINE_AT_EOF_CHECKER_H
#define FILE_CHECK_NEWLINE_AT_EOF_CHECKER_H


#include "specific/character_checker.h"
#include "utils/character.h"
#include <stdlib.h>


static inline
bool
newline_at_eof_checker_handler(int   a_character,
			       void* data)
{
  if(a_character == EOF)
    {
      return is_newline(*((char*) data));
    }
  *((int*) data) = a_character;
  return true;
}

static inline
bool
newline_at_eof_checker_error_printer(FILE* stream,
				     const void* data __attribute__((unused)))
{
  if(stream == NULL)
    {
      return false;
    }
  return
    fprintf(stream,
	    "There was no newline at the end of the file.\n")
    > 0;
}

static inline
character_checker_t
newline_at_eof_checker_create(void)
{
  character_checker_t checker;
  character_checker_init_first_time_only_unsafe(&checker);
  checker.data          = malloc(sizeof(int));
  checker.data_free     = free;
  checker.handler       = newline_at_eof_checker_handler;
  checker.error_printer = newline_at_eof_checker_error_printer;
  return checker;
}


#endif
