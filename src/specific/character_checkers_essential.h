/*
 * Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef FILE_CHECK_CHARACTER_CHECKERS_ESSENTIAL_H
#define FILE_CHECK_CHARACTER_CHECKERS_ESSENTIAL_H


#include "specific/character_checker.h"
#include "utils/vector_generic.h"


DEFINE_VECTOR_GENERIC_STATIC(character_checker_t)
typedef vector_character_checker_t character_checkers_t;


static inline
void
character_checkers_init_first_time_only(character_checkers_t* checkers)
{
  if(checkers != NULL)
    {
      VECTOR_GENERIC_STATIC_INIT_FIRST_TIME_ONLY_DEFAULT(checkers,
							 character_checker_t);
    }
}

static inline
void
character_checkers_destructor(character_checkers_t* checkers)
{
  for(size_t i=0; i < VECTOR_GENERIC_STATIC_GET_SIZE_UNSAFE(checkers); ++i)
    {
      character_checker_t* checker =
	VECTOR_GENERIC_STATIC_GET_ITEM_POINTER_UNSAFE(checkers, i);
      character_checker_destructor(checker);
    }
  VECTOR_GENERIC_STATIC_DESTRUCTOR_UNSAFE(checkers)
}


#endif
