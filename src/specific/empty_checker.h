/*
 * Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef FILE_CHECK_EMPTY_CHECKER_H
#define FILE_CHECK_EMPTY_CHECKER_H


#include "specific/character_checker.h"
#include <stdlib.h>


static inline
bool
empty_checker_handler(int   a_character,
		      void* data __attribute__((unused)))
{
  return a_character == EOF;
}

static inline
bool
empty_checker_error_printer(FILE* stream,
			    const void* data __attribute__((unused)))
{
  if(stream == NULL)
    {
      return false;
    }
  return
    fprintf(stream,
	    "There was at least a character.\n")
    > 0;
}

static inline
character_checker_t
empty_checker_create(void)
{
  character_checker_t checker;
  character_checker_init_first_time_only_unsafe(&checker);
  checker.handler       = empty_checker_handler;
  checker.error_printer = empty_checker_error_printer;
  return checker;
}


static inline
bool
not_empty_checker_handler(int   a_character,
			  void* data)
{
  if(*((bool*) data))
    {
      return true;
    }
  *((bool*) data) = true;
  return a_character != EOF;
}

static inline
bool
not_empty_checker_error_printer(FILE* stream,
			    const void* data __attribute__((unused)))
{
  if(stream == NULL)
    {
      return false;
    }
  return
    fprintf(stream,
	    "There was no character.\n")
    > 0;
}

static inline
bool
not_empty_checker_init(void* data)
{
  if(data == NULL)
    {
      return false;
    }
  *((bool*) data) = false;
  return true;
}

static inline
character_checker_t
not_empty_checker_create(void)
{
  character_checker_t checker;
  character_checker_init_first_time_only_unsafe(&checker);
  checker.data          = malloc(sizeof(bool));
  checker.data_free     = free;
  checker.init          = not_empty_checker_init;
  checker.handler       = not_empty_checker_handler;
  checker.error_printer = not_empty_checker_error_printer;
  return checker;
}


#endif
