/*
 * Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef FILE_CHECK_CHARACTER_CHECKER_H
#define FILE_CHECK_CHARACTER_CHECKER_H


#include <stdio.h>
#include "utils/bool.h"


typedef bool (*character_check_t)(int a_character);
typedef bool (*character_check_init_t)(void* data);
typedef bool (*character_check_handler_t)(int   a_character,
					  void* data);
typedef bool (*character_check_print_error_t)(FILE* stream,
					      const void* data);

struct character_checker_t
{
  character_check_init_t        init;
  character_check_handler_t     handler;
  character_check_print_error_t error_printer;
  void* data;
  void (*data_free)(void* this_data);
};
typedef struct character_checker_t character_checker_t;


static inline
void
character_checker_init_first_time_only_unsafe(character_checker_t* checker)
{
  checker->init          = NULL;
  checker->handler       = NULL;
  checker->error_printer = NULL;
  checker->data          = NULL;
  checker->data_free     = NULL;
}

static inline
bool
character_checker_init_unsafe(const character_checker_t* checker)
{
  return
    checker->init == NULL
    ? true
    : checker->init(checker->data);
}

static inline
bool
character_checker_init(const character_checker_t* checker)
{
  return
    checker == NULL
    ? false
    : character_checker_init_unsafe(checker);
}

static inline
bool
character_checker_handle_unsafe(const character_checker_t* checker,
				int a_character)
{
  return checker->handler(a_character, checker->data);
}

static inline
bool
character_checker_handle(const character_checker_t* checker,
			 int a_character)
{
  return
    checker != NULL &&
    checker->handler != NULL &&
    character_checker_handle_unsafe(checker, a_character);
}

static inline
bool
character_checker_print_error(const character_checker_t* checker,
			      FILE* stream)
{
  return
    checker != NULL &&
    checker->error_printer != NULL &&
    stream != NULL &&
    checker->error_printer(stream, checker->data);
}

static inline
void
character_checker_destructor(character_checker_t* checker)
{
  if(checker->data != NULL && checker->data_free != NULL)
    {
      checker->data_free(checker->data);
    }
}

static inline
bool
character_checker_equals_unsafe(const character_checker_t* checker1,
				const character_checker_t* checker2)
{
  return
    checker1->handler   == checker2->handler &&
    checker1->data      == checker2->data    &&
    checker1->data_free == checker2->data_free;
}


#endif
