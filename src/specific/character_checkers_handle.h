/*
 * Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef FILE_CHECK_CHARACTER_CHECKERS_HANDLE_H
#define FILE_CHECK_CHARACTER_CHECKERS_HANDLE_H


#include "character_checkers_essential.h"


static inline
bool
character_checkers_handle_character(character_checkers_t* checkers,
				    int a_character,
				    FILE* error_stream)
{
  const size_t checkers_nb =
    VECTOR_GENERIC_STATIC_GET_SIZE_UNSAFE(checkers);
  for(size_t i=0; i < checkers_nb; ++i)
    {
      const character_checker_t* checker =
	VECTOR_GENERIC_STATIC_GET_ITEM_POINTER_UNSAFE(checkers, i);
      if(!character_checker_handle_unsafe(checker, a_character))
	{
	  if(error_stream != NULL &&
	     !character_checker_print_error(checker, error_stream))
	    {
	      fprintf(error_stream,
		      "Error to print error of checker %zu / %zu\n",
		      i, checkers_nb);
	    }
	  return false;
	}
    }
  return true;
}

static inline
size_t
character_checkers_init(character_checkers_t* checkers)
{
  const size_t checkers_nb =
    VECTOR_GENERIC_STATIC_GET_SIZE_UNSAFE(checkers);
  for(size_t i=0; i < checkers_nb; ++i)
    {
      character_checker_t* checker =
	VECTOR_GENERIC_STATIC_GET_ITEM_POINTER_UNSAFE(checkers, i);
      if(!character_checker_init_unsafe(checker))
	{
	  return i;
	}
    }
  return checkers_nb;
}

static inline
bool
character_checkers_handle_stream(character_checkers_t* checkers,
				 FILE* a_stream,
				 const char* stream_name,
				 FILE* error_stream)
{
  const size_t character_checker_index_init_fail =
    character_checkers_init(checkers);
  if(character_checker_index_init_fail <
     VECTOR_GENERIC_STATIC_GET_SIZE_UNSAFE(checkers))
    {
      fprintf(stderr,
	      "Fail to init character checker %zu!\n",
	      character_checker_index_init_fail);
      return false;
    }
  
  while(true)
    {
      const int current_character = getc(a_stream);
      
      if(!character_checkers_handle_character(checkers,
					      current_character,
					      error_stream))
	{
	  return false;
	}
      
      if(current_character == EOF)
	{
	  if(ferror(a_stream))
	    {
	      if(error_stream != NULL)
		{
		  fprintf(error_stream,
			  "Error while reading %s\n", stream_name);
		}
	      return false;
	    }
	  return true;
	}
    }
}

static inline
bool
character_checkers_handle_file_with_name(character_checkers_t* checkers,
					 const char* file_name,
					 FILE* error_stream)
{
  FILE* file = fopen(file_name, "r");
  if(file == NULL)
    {
      if(error_stream != NULL)
	{
	  fprintf(error_stream,
		  "%s is not a file or can not be read.\n",
		  file_name);
	}
      return false;
    }
  
  if(!character_checkers_handle_stream(checkers,
				       file, file_name,
				       error_stream))
    {
      fclose(file);
      return false;
    }
  
  fclose(file);
  return true;
}


#endif
