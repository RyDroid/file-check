/*
 * Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef FILE_CHECK_NO_CARRIAGE_RETURN_CHECKER_H
#define FILE_CHECK_NO_CARRIAGE_RETURN_CHECKER_H


#include "specific/character_checker.h"
#include "utils/character.h"


static inline
bool
no_carriage_return_checker_handler(int   a_character,
				   void* data __attribute__((unused)))
{
  return is_not_carriage_return(a_character);
}

static inline
bool
no_carriage_return_checker_error_printer(FILE* stream,
					 const void* data __attribute__((unused)))
{
  if(stream == NULL)
    {
      return false;
    }
  return
    fprintf(stream,
	    "There was a carriage return.\n")
    > 0;
}

static inline
character_checker_t
no_carriage_return_checker_create(void)
{
  character_checker_t checker;
  character_checker_init_first_time_only_unsafe(&checker);
  checker.handler       = no_carriage_return_checker_handler;
  checker.error_printer = no_carriage_return_checker_error_printer;
  return checker;
}


#endif
