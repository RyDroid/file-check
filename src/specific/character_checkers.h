/*
 * Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef FILE_CHECK_CHARACTER_CHECKERS_H
#define FILE_CHECK_CHARACTER_CHECKERS_H


#include "character_checkers_handle.h"


static inline
character_checker_t*
character_checkers_get_pointer_unsafe(const character_checkers_t* checkers,
				      const character_checker_t*  a_checker)
{
  character_checker_t* current =
    VECTOR_GENERIC_STATIC_GET_BEGIN_POINTER(checkers);
  const character_checker_t* end =
    VECTOR_GENERIC_STATIC_GET_END_POINTER(checkers);
  while(current != end)
    {
      if(character_checker_equals_unsafe(current, a_checker))
	{
	  return current;
	}
      ++current;
    }
  return NULL;
}

static inline
bool
character_checkers_contains_unsafe(const character_checkers_t* checkers,
				   const character_checker_t*  a_checker)
{
  return
    character_checkers_get_pointer_unsafe(checkers, a_checker)
    != NULL;
}

static inline
bool
character_checkers_add(character_checkers_t* checkers,
		       character_checker_t   checker_to_add)
{
  if(character_checkers_contains_unsafe(checkers, &checker_to_add))
    {
      return false;
    }
  
  VECTOR_GENERIC_STATIC_PUSH_BACK(checkers,
				  checker_to_add,
				  character_checker_t);
  return true;
}


#endif
