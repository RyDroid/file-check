/*
 * Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef FILE_CHECK_CHARACTER_H
#define FILE_CHECK_CHARACTER_H


#include "bool.h"


static inline
bool
is_ascii_character(int a_character)
{
  return a_character >= 0 && a_character <= 127;
}

static inline
bool
is_not_carriage_return(int a_character)
{
  return a_character != '\r';
}

static inline
bool
is_newline(int a_character)
{
  return a_character == '\n' || a_character == '\r';
}


#endif
