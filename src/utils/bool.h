/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


/**
 * @file
 * @brief stdbool.h of C99 and C11 contains the same things.
 */


#ifndef FILE_CHECK_BOOL_H
#define FILE_CHECK_BOOL_H

#ifndef __bool_true_false_are_defined

#ifndef __cplusplus

#define false 0
#define true  1
#define FALSE 0
#define TRUE  1
typedef char bool;

#endif /* __cplusplus */

#endif /* __bool_true_false_are_defined */

#ifndef BOOL_EQUALS
/* Two bool values are compared directly using a C primitive.
 * This may produce unexpected results since all non-zero values
 * are considered true, so different true values may not be equal.
 */
#define BOOL_EQUALS(a, b) ((a) ? (b) : !(b))
#endif

#endif
