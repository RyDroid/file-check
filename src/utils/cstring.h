/*
 * Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef FILE_CHECK_CSTRING_H
#define FILE_CHECK_CSTRING_H


#include <string.h>
#include <stdio.h>
#include "bool.h"


typedef char* cstring_t;


static inline
bool
cstring_equals_unsafe(const char* str1,
		      const char* str2)
{
  return strcmp(str1, str2) == 0;
}

static inline
int
cstring_to_int(const char* str, bool* success)
{
  int number;
  *success = sscanf(str, "%5d", &number) == 1;
  return number;
}


#endif
