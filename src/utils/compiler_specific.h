/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


/**
 * @file
 * @brief Some compiler specifics things.
 */


#ifndef FILE_CHECK_COMPILER_SPECIFIC_H
#define FILE_CHECK_COMPILER_SPECIFIC_H


/**
 * @brief If we're not using GNU C, elide __attribute__
 */
#if !defined(__GNUC__) && !defined(__attribute__)
#  define  __attribute__(x)  /* GNU __attribute__ */
#endif


#endif
